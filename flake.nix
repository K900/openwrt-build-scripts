{
  description = "OpenWRT build environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    flake-utils.url = "github:numtide/flake-utils";

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      pre-commit-hooks,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = pkgs.mkShell {
          packages = with pkgs; [
            just

            cacert
            envsubst
            file
            getopt
            git
            gcc
            gnumake
            ncurses
            perl
            pkg-config
            (python3.withPackages (ps: [ ps.setuptools ]))
            rsync
            swig
            unzip
            wget
            which

            # for bpf
            (symlinkJoin {
              name = "openwrt-bpf-env";
              paths = with llvmPackages_latest; [
                clang-unwrapped
                llvm
              ];
            })
          ];

          # needed for LZMA to build
          hardeningDisable = [ "format" ];

          inherit (self.checks.${system}.pre-commit-check) shellHook;
        };

        checks = {
          pre-commit-check = pre-commit-hooks.lib.${system}.run {
            src = ./.;
            hooks = {
              nixfmt-rfc-style.enable = true;
              deadnix.enable = true;
              shellcheck.enable = true;
              shfmt.enable = true;
              statix = {
                enable = true;
                settings.ignore = [ "src/*" ];
              };
            };
          };
        };
      }
    );
}
