build TARGET="ax6s":
    @nix --extra-experimental-features "nix-command flakes" develop --ignore-environment --command ./build.sh {{TARGET}}

clean:
    @make -C src clean
    @./src/scripts/dl_cleanup.py --download-dir=./dl/ --build-dir=./src/build_dir/

_flash TARGET="ax6s":
    @scp -O bin/{{TARGET}}/targets/*/*/jankwrt-*-squashfs-sysupgrade.itb root@192.168.0.1:/tmp/fw.bin
    @ssh root@192.168.0.1 'sysupgrade -v /tmp/fw.bin'

flash TARGET="ax6s": (build TARGET) (_flash TARGET)
