#!/usr/bin/env bash
set -euo pipefail
shopt -s nullglob

ROOT_DIR=$(dirname "$(readlink -f "$0")")
export ROOT_DIR
cd "${ROOT_DIR}"

TARGET=$1
export TARGET

CONFIG_COMMIT=$(git rev-parse --short HEAD)
export CONFIG_COMMIT
BUILD_DATE=$(date +'%Y.%m.%d.%H.%M.%S')
export BUILD_DATE

echo "=== Updating source..."
cd src/
git fetch
git reset --hard origin/master

OWRT_COMMIT=$(git rev-parse --short HEAD)
export OWRT_COMMIT

echo "=== Patching..."
export GIT_COMMITTER_NAME="Consistent Patch Applier"
export GIT_COMMITTER_EMAIL="noreply@0upti.me"

maybe_patch() {
	if [ "$#" -gt 0 ]; then
		git am "$@" --whitespace=nowarn --committer-date-is-author-date
	fi
}

maybe_patch ../patches/*.patch
maybe_patch ../patches/"${1}"/*.patch

echo "=== Updating feeds..."
cp feeds.conf.default feeds.conf
echo "src-link custom ${ROOT_DIR}/packages" >>feeds.conf
echo "src-git amnezia https://github.com/K900/amneziawg-openwrt.git;patch-1" >>feeds.conf
./scripts/feeds update -a
./scripts/feeds install -a

echo "=== Generating configuration..."
gcc -E -x c "${ROOT_DIR}/targets/${1}.conf" | envsubst >.config

make defconfig

echo "=== Downloading deps..."
make -j8 download

echo "=== Building with $(nproc) workers..."
make "-j$(nproc)"

echo "=== Resetting source..."
git reset --hard origin/master
